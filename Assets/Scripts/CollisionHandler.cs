using UnityEngine;
using UnityEngine.SceneManagement;

public class CollisionHandler : MonoBehaviour
{
    [SerializeField] private float deathDelay = 1f;
    [SerializeField] private ParticleSystem deathParticles;
    private void OnTriggerEnter(Collider other)
    {
        HandleDeath();
    }

    void HandleDeath()
    {
        GetComponent<Controls>().enabled = false;
        GetComponent<MeshRenderer>().enabled = false;

        foreach (Collider _collider in GetComponentsInChildren<Collider>())
        {
            _collider.enabled = false;
        }
        
        deathParticles.Play();
        Invoke(nameof(ReloadLevel), deathDelay);
    }

    void ReloadLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}