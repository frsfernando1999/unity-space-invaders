using UnityEngine;
using UnityEngine.InputSystem;

public class Controls : MonoBehaviour
{
    [Header("Inputs")] [SerializeField] private InputAction movement;
    [SerializeField] private InputAction firing;

    [Header("Weapons")] [SerializeField] private GameObject[] lasers;

    [Header("Speed and Camera Lock")] [Tooltip("How fast the ship goes up and down")] [SerializeField]
    private float flySpeed = 20f;

    [Tooltip("Locks the ship on the horizontal axis")] [SerializeField]
    private float xRange = 15f;

    [Tooltip("Locks the ship on the vertical axis")] [SerializeField]
    private float yRange = 12f;

    [Header("Screen position based on tuning")] [SerializeField]
    private float positionPitchFactor = 1f;

    [SerializeField] private float controlPitchFactor = -10f;
    [SerializeField] private float positionYawFactor = -1f;
    [SerializeField] private float controlRowFactor = -10f;

    [Tooltip("Rotation interpolation speed")] [SerializeField] [Range(0, 1)]
    private float rotationFactor = 0.5f;

    private float _xMovement;
    private float _yMovement;

    private void OnEnable()
    {
        movement.Enable();
        firing.Enable();
    }

    private void OnDisable()
    {
        movement.Disable();
        firing.Disable();
    }

    void Update()
    {
        ProcessTranslation();
        ProcessRotation();
        ProcessFiring();
    }

    private void ProcessFiring()
    {
        ToggleLasers(firing.IsPressed());
    }

    private void ToggleLasers(bool toggle)
    {
        foreach (GameObject laser in lasers)
        {
            ParticleSystem.EmissionModule emission = laser.GetComponent<ParticleSystem>().emission;
            emission.enabled = toggle;
        }
    }

    private void ProcessRotation()
    {
        var localTransform = transform.localPosition;
        float pitch = localTransform.y * positionPitchFactor + (_yMovement * controlPitchFactor);
        float yaw = localTransform.x * positionYawFactor;
        float row = _xMovement * controlRowFactor;

        Quaternion targetRotation = Quaternion.Euler(pitch, yaw, row);

        transform.localRotation = Quaternion.RotateTowards(transform.localRotation, targetRotation, rotationFactor);
    }

    private void ProcessTranslation()
    {
        _xMovement = movement.ReadValue<Vector2>().x;
        _yMovement = movement.ReadValue<Vector2>().y;

        var localPosition = transform.localPosition;

        float newXPosition = localPosition.x + (_xMovement * Time.deltaTime * flySpeed);
        float xClamped = Mathf.Clamp(newXPosition, -xRange, xRange);

        var newYPosition = localPosition.y + (_yMovement * Time.deltaTime * flySpeed);
        float yClamped = Mathf.Clamp(newYPosition, -yRange, yRange);

        transform.localPosition =
            new Vector3(
                xClamped,
                yClamped,
                localPosition.z);
    }
}