using System;
using TMPro;
using UnityEngine;

public class ScoreBoard : MonoBehaviour
{
    private float _score;
    private TMP_Text scoreText;


    private void Start()
    {
        scoreText = GetComponent<TMP_Text>();
        scoreText.text = "Start";
    }

    public void UpdateScore(float addScore)
    {
        _score += addScore;
        scoreText.text = _score.ToString();
    }

    public float GetScore()
    {
        return _score;
    }
}