using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private GameObject deathVFX;
    [SerializeField] private GameObject hitVFX;
    [SerializeField] private float killScore = 1000f;
    [SerializeField] private float hitScore = 50f;
    [SerializeField] private float health = 100f;
    [SerializeField] private float safetyDestructionOffset = 0.5f;


    private ScoreBoard _scoreBoard;

    private void Start()
    {
        var _rigidbody = gameObject.AddComponent<Rigidbody>();
        _rigidbody.useGravity = false;
        _scoreBoard = FindObjectOfType<ScoreBoard>();
    }   

    private void OnParticleCollision(GameObject other)
    {
        _scoreBoard.UpdateScore(hitScore);
        health -= other.GetComponent<PlayerDamage>().weaponDamage;
        var hitParticles = Instantiate(hitVFX, transform.position, Quaternion.identity);
            
        var destroyDelay = hitParticles.GetComponent<ParticleSystem>().main.startLifetime.constantMax + safetyDestructionOffset;
        Destroy(hitParticles, destroyDelay);
        
        if (health <= 0)
        {
            HandleDestruction();
        }
    }

    private void HandleDestruction()
    {
        var deathParticles = Instantiate(deathVFX, transform.position, Quaternion.identity);
        var destroyDelay = deathParticles.GetComponent<ParticleSystem>().main.startLifetime.constantMax +
                           safetyDestructionOffset;
        Destroy(deathParticles, destroyDelay);
        _scoreBoard.UpdateScore(killScore);
        Destroy(gameObject);
    }
}